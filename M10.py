# -*- coding: utf-8 -*-
"""
Created on Fri Sep 22
Get the latest version at:
https://gitlab.com/LeoBras/sleep_analisys

@author: Leonardo Bras Soares Passos (@leobras)
"""


import csv
import os
import sys

original_stdout = sys.stdout

res = []
for file in os.listdir(r'.'):
    if file.endswith('.ebe'):
        res.append(file)

saida = open('M10_output.txt', 'w')

samples_per_min = 10
size_24h = 24 * 60 * samples_per_min
size_10h = 10 * 60 * samples_per_min

for filename in res:
    print(filename)

    file = open(filename, newline='');
    csvreader = csv.reader(file)

    # allocate max data size
    c = 0
    data = [0] * size_24h

    #parse data to data array
    for row in csvreader:
        if (c >= size_24h):
            break

        # Awake = 1, stays 0 othewise.
        if (row[3] != '0'):
            data[c] = 1

        c = c + 1

    # Continue only if there is enough data.
    if (c < size_10h):
        sys.stdout = saida
        print("%s : not enough data. Points counted: %d" % (filename, c))
        sys.stdout = original_stdout
        file.close()
        continue

    max_idx = 0
    max_value = 0
    current_value = 0

    for i in range(c):
        current_value = current_value + data[i]

        if (i < size_10h):
            continue

        if (max_value < current_value):
            max_value = current_value
            max_idx = i

        # Remove last value, so the data window shifts one position
        current_value = current_value - data[i - size_10h]

    second_end = (max_idx % samples_per_min) * (60 / samples_per_min)
    minute_end = (max_idx % (60 * samples_per_min)) // samples_per_min
    hour_end = max_idx // (60 * samples_per_min)

    sys.stdout = saida
    print("%s : M10 %02d:%02d:%02d - %02d:%02d:%02d @ %f%%" % (filename, (hour_end - 5) % 24, minute_end, second_end, hour_end, minute_end, second_end, (100 * max_value) / size_10h))
    sys.stdout = original_stdout

    file.close()

saida.close()
