# -*- coding: utf-8 -*-
"""
Created on Sat Aug  5 18:43:41 2023
Get the latest version at:
https://gitlab.com/LeoBras/sleep_analisys

@author: Leonardo Bras Soares Passos (@leobras)
"""


import csv
import os
import sys

original_stdout = sys.stdout

res = []
for file in os.listdir(r'.'):
    if file.endswith('.ebe'):
        res.append(file)
        
saida = open('reg_output.txt', 'w')


for filename in res:
    print(filename)
     
    file = open(filename, newline='');
    csvreader = csv.reader(file)
    
    total = 0
    sleeping = 0
    
    for row in csvreader:
        if (total > 14400):	#24 hours x 60 minutes x 10 points/min
            break
        total = total + 1
        if (row[3] == '0') :
            sleeping = sleeping + 1
    
    sys.stdout = saida
    print("%s : %f%%" % (filename, 100 * sleeping / total))
    sys.stdout = original_stdout
    
    file.close()
    
saida.close()
