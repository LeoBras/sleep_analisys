# -*- coding: utf-8 -*-
"""
Created on Fri Sep 22
Get the latest version at:
https://gitlab.com/LeoBras/sleep_analisys

@author: Leonardo Bras Soares Passos (@leobras)
"""


import csv
import os
import sys

original_stdout = sys.stdout

res = []
for file in os.listdir(r'.'):
    if file.endswith('.ebe'):
        res.append(file)

saida = open('L5_output.txt', 'w')

samples_per_min = 10
size_24h = 24 * 60 * samples_per_min
size_5h = 5 * 60 * samples_per_min

for filename in res:
    print(filename)

    file = open(filename, newline='');
    csvreader = csv.reader(file)

    # allocate max data size
    c = 0
    data = [0] * size_24h

    #parse data to data array
    for row in csvreader:
        if (c >= size_24h):
            break

        # Awake = 1, stays 0 othewise.
        if (row[3] != '0'):
            data[c] = 1

        c = c + 1

    # Continue only if there is enough data.
    if (c < size_5h):
        sys.stdout = saida
        print("%s : not enough data. Points counted: %d" % (filename, c))
        sys.stdout = original_stdout
        file.close()
        continue

    min_idx = 0
    min_value = size_5h #Makes sure to always be replaced on first test
    current_value = 0

    for i in range(c):
        current_value = current_value + data[i]

        if (i < size_5h):
            continue

        if (min_value > current_value):
            min_value = current_value
            min_idx = i

        # Remove last value, so the data window shifts one position
        current_value = current_value - data[i - size_5h]

    second_end = (min_idx % samples_per_min) * (60 / samples_per_min)
    minute_end = (min_idx % (60 * samples_per_min)) // samples_per_min
    hour_end = min_idx // (60 * samples_per_min)

    sys.stdout = saida
    print("%s : L5 %02d:%02d:%02d - %02d:%02d:%02d @ %f%%" % (filename, (hour_end - 5) % 24, minute_end, second_end, hour_end, minute_end, second_end, (100 * min_value) / size_5h))
    sys.stdout = original_stdout

    file.close()

saida.close()
